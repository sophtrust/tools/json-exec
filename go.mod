module go.sophtrust.dev/json-exec

go 1.16

require (
	github.com/fsnotify/fsnotify v1.4.7
	github.com/rs/zerolog v1.21.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.0
	go.sophtrust.dev/pkg/zerolog v1.26.2
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
